# Expanded Materials-Concrete Barricade

Why make a simple wooden barricade when concrete is available and a stronger and quick setting material.

## Content

This mod adds a fortified barricade to the security tab:

	- standard size (1x1)
	
	- extended Hitpoints: 440
	
	- quicker to build: 220 work
	
	- flameproof

Unlockable with research after concrete making.

Also patches in a variant of the barricade that can be built out of rock powder, no stat differences.

## Requirements
The following mods are required:

	- [Expanded Materials - Metals](https://steamcommunity.com/workshop/filedetails/?id=2259837114)

[Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2401400212)

## Version 1.1


## Donate

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/C0C43PQ0I)